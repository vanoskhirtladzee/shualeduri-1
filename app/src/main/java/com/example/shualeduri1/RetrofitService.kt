package com.example.shualeduri1

import retrofit2.Call
import retrofit2.http.GET

interface RetrofitService {
    @GET("cars_list.json")
    fun getCarList(): Call<MutableList<Car>>
}