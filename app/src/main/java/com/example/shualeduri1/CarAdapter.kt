package com.example.shualeduri1

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_car_item.view.*

class CarAdapter(private val context: Context, private val carList: MutableList<Car>): RecyclerView.Adapter<CarAdapter.MyViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var itemView = LayoutInflater.from(context).inflate(R.layout.layout_car_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return carList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Picasso.get().load(carList[position].image).into(holder.image)
        holder.txt_name.text=carList[position].name
        holder.txt_desc.text=carList[position].desc
    }
    class MyViewHolder(itemView:View): RecyclerView.ViewHolder(itemView){
        var image:ImageView
        var txt_name:TextView
        var txt_desc:TextView

        init {
            image = itemView.image_Car
            txt_name = itemView.txt_name
            txt_desc = itemView.txt_desc

        }

    }
}