package com.example.shualeduri1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main_recycler_view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainRecyclerView : AppCompatActivity() {

    lateinit var mService: RetrofitService
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: CarAdapter
    lateinit var dialog: AlertDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_recycler_view)


        mService = Common.retrofitService

        recyclerCarList.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        recyclerCarList.layoutManager = layoutManager



        getAllCarList()


    }

    private fun getAllCarList() {
        mService.getCarList().enqueue(object : Callback<MutableList<Car>> {
            override fun onFailure(call: Call<MutableList<Car>>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<MutableList<Car>>,
                response: Response<MutableList<Car>>
            ) {
                adapter = CarAdapter(baseContext, response.body() as MutableList<Car>)
                adapter.notifyDataSetChanged()
                recyclerCarList.adapter = adapter


            }

        })


    }
}