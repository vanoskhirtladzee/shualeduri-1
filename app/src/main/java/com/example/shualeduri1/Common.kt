package com.example.shualeduri1

object Common {
    private val BASE_URL = "https://navneet7k.github.io/"

    val retrofitService:RetrofitService
    get() = RetrofitClient.getClient(BASE_URL).create(RetrofitService::class.java)
}