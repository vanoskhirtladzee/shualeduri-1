package com.example.shualeduri1

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_dashrboard.*


class DashboardActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashrboard)

        auth = FirebaseAuth.getInstance()

        homePageButton.setOnClickListener {
            homeButton()

        }

        carList.setOnClickListener {
            startActivity(Intent(this,MainRecyclerView::class.java))
            finish()
        }



        }
    private fun homeButton() {
        auth.signOut()
        startActivity(Intent(this,MainActivity::class.java))
        Toast.makeText(baseContext, "Signing Out",
            Toast.LENGTH_SHORT).show()
        finish()
    }

    }

